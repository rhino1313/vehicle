App = React.createClass({
  mixins: [ReactMeteorData],

  clearAll () {
    Meteor.call('clearAll');
  },

  addVehicle () {
    var vid = React.findDOMNode(this.refs.vehicle_id).value;
    React.findDOMNode(this.refs.vehicle_id).value = '';

    if ( !_.isEmpty(vid) ) {
      if ( !this.hasVehicle(vid) ) {
        Vehicles.insert({vid: vid});
      } else {
        this.shakeVehicle(vid, false);
      }
    } else {
      sweetAlert("Oops...", "enter id for vehicle", 'error');
    }
  },

  addPart () {
    var pid = React.findDOMNode(this.refs.part_id).value;
    var vid = React.findDOMNode(this.refs.part_vehicle_id).value;
    React.findDOMNode(this.refs.part_id).value = '';
    React.findDOMNode(this.refs.part_vehicle_id).value = '';

    if ( _.isEmpty(vid) || _.isEmpty(pid) ) {
      sweetAlert("Oops...","enter id's for part and vehicle",'error');
    } else if ( !this.hasVehicle(vid) ) {
      sweetAlert("Oops...", 'add vehicle first', 'error');
    } else if ( this.hasPart(pid) ) {
      this.shakeVehicle(pid, true);
    } else {
      Parts.insert({
                    pid: pid,
                    vid: vid
                  });
    }
  },

  hasPart (pid) {
    return !_.isEmpty(Parts.findOne({pid: pid}));
  },

  hasVehicle (vid) {
    return !_.isEmpty(Vehicles.findOne({vid: vid}));
  },

  shakeVehicle (id, isPart) {
    var vid =  !isPart ? id : Parts.findOne({pid: id}).vid;

    this.setState({
      shake: vid
    });

    _.delay(this.stopShake, 500);
  },

  stopShake () {
    this.setState({
      shake: false
    })
  },

  getInitialState () {
    return {
      shake: false
    }
  },

  getMeteorData () {
    var vehicles = Vehicles.find({}).fetch();
    var parts = Parts.find({}).fetch();

    if ( !_.isEmpty(parts) ) {
      vehicles = vehicles.map((v) => {
        var vparts = parts.filter((p) => {
          return v.vid == p.vid ? true : false;
        });

        v.parts = vparts;
        return v;
      });
    }

    return {
      vehicles: vehicles,
    };
  },

  render () {
    var vehicles = this.data.vehicles;
    var shake = this.state.shake;

    return (
      <div className="app">
        <div className="menu">
          <button className="clear-all btn btn-danger pull-right"
                  onClick={this.clearAll}>Clear All</button>
          <br></br>
          <label> Enter new vehicle id:</label>
          <br></br>
          <input type="text"
                 className="vehicle-id form-control"
                 placeholder="enter vehicle id"
                 ref="vehicle_id"/>
          <button className="add-vehicle btn btn-default"
                  onClick={this.addVehicle}>Add Vehicle</button>
          <br></br>
          <label> Enter new part id and id of existing vehicle:</label>
          <br></br>
          <input type="text"
                 className="part-id form-control"
                 placeholder="enter part id"
                 ref="part_id" />
          <input type="text"
                 className="part-vehicle-id form-control"
                 placeholder="enter vehicle id for this part"
                 ref="part_vehicle_id" />
          <button className="add-part btn btn-default"
                  onClick={this.addPart}>Add Part</button>
        </div>

        <div className="vehicles-container">
          {!_.isEmpty(vehicles) && vehicles.map((v, key) => {
            return (<Vehicle key={key}
                             vid={v.vid}
                             parts={v.parts}
                             shake={v.vid == shake ? 'shake' : ''}/>);
          })}
        </div>
      </div>
    );
  }
});

Meteor.startup(() => {
  React.render((<App />), document.getElementById('render-app'));
});
