Vehicle = React.createClass({
  removeVehicle () {
    Meteor.call('removeVehicle', this.props.vid, this.props.parts);
  },

  render () {
    var vid = this.props.vid;
    var parts = this.props.parts;
    var shake = this.props.shake;

    return (
      <div className={"vehicle " + shake}>
        <h3>Vehicle id: {vid}</h3>
        {!_.isEmpty(parts) && parts.map((p, key) => {
          return (<Part key={key}
                        vid={p.vid}
                        pid={p.pid} />);
        })}

        <button className="remove-vehicle btn btn-danger"
                onClick={this.removeVehicle}>X</button>
      </div>
    );
  }
});
