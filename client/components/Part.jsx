Part = React.createClass({
  render () {
    var pid = this.props.pid;

    return (
      <div className="part">Part id: {pid}</div>
    );
  }
});
