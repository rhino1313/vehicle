Meteor.methods({
  clearAll () {
    Vehicles.remove({});
    Parts.remove({});
  },

  removeVehicle (vid, parts) {
    parts = _.pluck(parts, 'pid');
    Parts.remove({pid: {$in: parts}});
    Vehicles.remove({vid: vid});
  }
});
